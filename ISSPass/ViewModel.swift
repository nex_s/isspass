//
//  ViewModel.swift
//  ISSPass
//
//  Created by nsn on 1/12/18.
//  Copyright © 2018 nex sn. All rights reserved.
//

import UIKit

struct ISSPass: Decodable {
    let message: String?
    let request: Request?
    let response: [Response]?

    init(message: String? = nil, request : Request? = nil, response : [Response]? = nil) {
        self.message = message
        self.request = request
        self.response = response
    }
}

struct Request : Decodable {
    let altitude : Int?
    let datetime : Double?
    let latitude : Double?
    let longitude : Double?
    let passes : Int?
}

struct Response : Decodable {
    let duration : Int?
    let risetime : Double?
}

class ViewModel: NSObject {

    @IBOutlet weak var services: Services!
    var iSSPass: ISSPass = ISSPass()

    func fetchISSPassesData(forAltitude alt:String, latitude lat:String, longitude lon:String, passes pas:String, completion:@escaping () -> ()){
        services.fetchISSPassesData(forAltitude: alt, latitude: lat, longitude: lon, passes: pas, completion: {iSSPass in
            self.iSSPass = iSSPass
            completion()
        })
    }
}
