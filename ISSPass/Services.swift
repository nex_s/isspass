//
//  Services.swift
//  ISSPass
//
//  Created by nsn on 1/12/18.
//  Copyright © 2018 nex sn. All rights reserved.
//

import UIKit
class Services: NSObject {
let urlString = "http://api.open-notify.org/iss-pass.json?"
    //lat=10&lon=10&n=6&alt=100

    func fetchISSPassesData(forAltitude alt:String, latitude lat:String, longitude lon:String, passes pas:String, completion:@escaping (ISSPass) -> ())  {
        guard let url = URL(string: urlString + "lat=\(lat)&lon=\(lon)&n=\(pas)&alt=\(alt)") else { return }

        URLSession.shared.dataTask(with: url) { (data, response, err) in
        guard let data = data else { return }
            do {
                let iSSPass = try JSONDecoder().decode(ISSPass.self, from: data)
                completion(iSSPass)
                return
            } catch let err {
                print("Error serializing json:", err)
                // TO DO .. hundle all posible errors
            }
            }.resume()

    }
}
